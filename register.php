
<?php
	ini_set("session.cookie_httponly", 1);
	require 'database.php';
	header("Content-Type: application/json");
	
	if(isset($_POST['username']) && isset($_POST['password']) && ($_POST['password']==$_POST['checkpass'])){
		$username = $_POST['username'];
		$password = crypt($_POST['password']);
		$check = $mysqli->prepare("select count(*) from login where username = ?");
		if(!$check){
			echo json_encode(array(
				"success" => false,
				"message" => "an error occured, please try again"
			));
			exit;
		}
		$check->bind_param('s', $username);
		$check->execute();
		$check->bind_result($count);
		$check->fetch();
		$check->close();
		if($count != 0){
			echo json_encode(array(
				"success" => false,
				"message" => "That user already exists!"
				));
			exit;
		}
		else{
			$stmt = $mysqli->prepare("insert into login (username, password) values (?,?)");
			if(!$stmt){
				printf("error: %s\n", $mysqli->error);
				exit;
			}
			$stmt->bind_param('ss', $username, $password);
			$stmt->execute();
			$stmt->close();
			echo json_encode(array(
				"success" => true,
				"message" => "registration successful, you can now log in!"
			));
			exit;
		}
	}
	else{
		echo json_encode(array(
			"success" => false,
			"message" => "check if all fields are correct"
	));
	}
?>

