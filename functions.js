// Hello.
//
// This is JSHint, a tool that helps to detect errors and potential
// problems in your JavaScript code.
//
// To start, simply enter some JavaScript anywhere on this page. Your
// report will appear on the right side.
//
// Additionally, you can toggle specific options in the Configure
// menu.

 
 /* load the user's calendar and set the options in the select form input */
function loadCalendars(){
	var dataString = '';
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "getCalendars.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText);
		if(jsonData.length > 0){
			$.each(jsonData, function(key, value){
				$('.calendars').append('<option value="'+value+'">'+value+'</option>');
			});
		}
	}, false);
	xmlHttp.send(dataString);
}
//remove unnecessary content
function removeCal(){
	$('.calendars').remove();
	$('.calOptionsHome').append('<select class="calendars" id="calendarsHome"><option value="default">--</option></select>');
	$('.calOptionsPopUp').append('<select class="calendars" id="calendarsPopUp"><option value="default">--</option></select>');
}

function deleteCalendar(event){
	var deleteCal = document.getElementById("calendarsHome").options[document.getElementById("calendarsHome").selectedIndex].value;
	if(deleteCal == 'default'){
		alert("This calendar cannot be deleted!");
		return;
	}
	
	else{
		var dataString = "delete_cal="+encodeURIComponent(deleteCal);
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open("POST", "deleteCalendar.php", true);
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xmlHttp.addEventListener("load", function(event){
			var jsonData = JSON.parse(event.target.responseText);
			if(jsonData.success){
				removeCal();
				loadCalendars();
				loadEvents();
				alert("Success");
			}
			else{
				alert(jsonData.message);
			}
		}, false);
		xmlHttp.send(dataString);
	}
}

$(function(){
	document.getElementById('deleteCal').addEventListener("click", deleteCalendar, false);
});


$(function(){
	$('#loginButton').click(function(event){
		if($('#loginPopUp').css('display') == 'none'){
			$('#loginPopUp').css('display', 'block');
		}
	});
});


$(function(){
	$('#cancelLogin').click(function(event){
		if($('#loginPopUp').css('display') == 'block'){
			$('#loginPopUp').css('display', 'none');
		}
	});
});


$(function(){
	$('#regButton').click(function(event){
		if($('#regPopUp').css('display') == 'none'){
			$('#regPopUp').css('display', 'block');
		}
	});
});


$(function(){
	$('#cancelreg').click(function(event){
		if($('#regPopUp').css('display') == 'block'){
			$('#regPopUp').css('display', 'none');
		}
	});
});
/* send ajax request to register */
function registerUser(event){

	var username = document.getElementById("user").value;
	var password = document.getElementById("pass").value;
	var checkpass = document.getElementById("repassword").value;
	
	var dataString = "username="+encodeURIComponent(username)+"&password="+encodeURIComponent(password)+"&checkpass="+encodeURIComponent(checkpass);
	
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "register.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText);
		if(jsonData.success){
			alert("Success");
		}
		else{
			alert(jsonData.message);
		}
	}, false);
	xmlHttp.send(dataString);
}

/* call the registerUser function (above) when the register button is clicked */
$(function(){
	document.getElementById("register").addEventListener("click", registerUser, false);
});

/* ajax request to login */
function loginUser(event){
	var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;
	
	
	var dataString = "username="+encodeURIComponent(username)+"&password="+encodeURIComponent(password);

	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "login.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText);
		if(jsonData.success){
			/* reset the form inputs */
			document.getElementById('username').value = '';
			document.getElementById('password').value = '';
			$('#loginPopUp').css('display', 'none');
			$('#loginButton').css('display', 'none');
			$('#logoutButton').css('display', 'block');
			/* set the welcome div to display a hello message */
			$('#welcome').html('');
			$('#welcome2').html('You are currently logged in as ' + jsonData.username + '!');
			loadCalendars();
			loadEvents();
		}
		else{
			alert(jsonData.message);
		}
	}, false);
	xmlHttp.send(dataString);
}

$(function(){
	document.getElementById("loginFormButton").addEventListener("click", loginUser, false);
});

function logoutUser(event){
	var dataString = '';
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "logout.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText);
		if(jsonData.success){
			/* display the login button on successful logout */
			$('#logoutButton').css('display', 'none');
			$('#loginButton').css('display', 'block');
			/* remove welcome text */
			$('#welcome').html('You are not logged in!');
			$('#welcome2').html("You won't be able to edit");
			hideEvents();
			removeCal();
			alert("Success");
		}
		else{
			alert("Success");
		}
	}, false);
	xmlHttp.send(dataString);
}

$(function(){
	document.getElementById("logoutButton").addEventListener("click", logoutUser, false);
});

/* popup the add event window */
$(function(){
	$('.addevent').click(function(event){
		if($('#addEventPopUp').css('display') == 'none'){
			$('#addEventPopUp').css('display', 'block');
		}
	});
});

/* reset form fields and hide the add event form when the cancel button is clicked on the add event form */
$(function(){
	$('#cancelAddEvent').click(function(event){
		if($('#addEventPopUp').css('display') == 'block'){
			document.getElementById('event_name').value = '';
			document.getElementById('event_date').value = '';
			document.getElementById('event_time').value = '';
			document.getElementById('event_desc').value = '';
			document.getElementById('newCal').value = '';
			$('#addEventPopUp').css('display', 'none');
		}
	});
});

/* ajax request */
function addEvent(event){
	var event_name = document.getElementById("event_name").value;
	var event_date_temp = document.getElementById("event_date").value;
	var event_time = document.getElementById("event_time").value;
	var event_desc = document.getElementById("event_desc").value;
	var cal_name = document.getElementById("calendarsPopUp").options[document.getElementById("calendarsPopUp").selectedIndex].value;
	var new_cal_name = document.getElementById("newCal").value;
	var event_date_array = event_date_temp.split("/");
	if(event_date_array.length == 3){
		var date_year = event_date_array[2];
		var date_month = event_date_array[0];
		var date_day = event_date_array[1];
	}
	else{
		document.getElementById("event_Date").value = '';
		alert("check date format");
	}
	
	/* check for appropriate input */
	if(event_name == "" || isNaN(date_year) || isNaN(date_month) || isNaN(date_day) || event_time === ""){
		alert("Title, date and time cannot be blank!");
		return;
	}
	/* new calendar */
	if(new_cal_name != ''){
		var dataString = "new_cal="+encodeURIComponent(new_cal_name)+"&event_name="+encodeURIComponent(event_name)+"&date_year="+encodeURIComponent(date_year)+"&date_month="+encodeURIComponent(date_month)+"&date_day="+encodeURIComponent(date_day)+"&event_time="+encodeURIComponent(event_time)+"&event_desc="+encodeURIComponent(event_desc);
	}
	/* if input goes into existing calendar*/
	else{
		var dataString = "cal_name="+encodeURIComponent(cal_name)+"&event_name="+encodeURIComponent(event_name)+"&date_year="+encodeURIComponent(date_year)+"&date_month="+encodeURIComponent(date_month)+"&date_day="+encodeURIComponent(date_day)+"&event_time="+encodeURIComponent(event_time)+"&event_desc="+encodeURIComponent(event_desc);
	}
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "addEvent.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText);
		if(jsonData.success){
			/* reset form fields and hide popups */
			document.getElementById('event_name').value = '';
			document.getElementById('event_date').value = '';
			document.getElementById('event_time').value = '';
			document.getElementById('event_desc').value = '';
			document.getElementById('newCal').value = '';
			$('#addEventPopUp').css('display', 'none');
			/* update calendar info */
			hideEvents();
			loadEvents();
			removeCal();
			loadCalendars();
			alert("Success");
		}
		else{
			document.getElementById('password').value = '';
			alert(jsonData.message);
		}
	}, false);
	xmlHttp.send(dataString);
}


/* calls addEvent function (above) when the add event button is clicked on the form */
$(function(){
	document.getElementById("addEventButton").addEventListener("click", addEvent, false);
})


$(function(){
	$('#prevmonth').click(function(event){
		var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var monthYear = $('#monthTitle').html().split(" ");
		var thisMonth = month.indexOf(monthYear[0]);
		hideEvents();
		removeCalendar();
		if(thisMonth - 1 < 0){
			monthCounter=11;
			yearCounter--;
			$('#monthTitle').html(month[11] + " " + yearCounter);
			$('#prevmonth').html("<<<|    ");
			$('#nextmonth').html("    |>>>");
		}
		else{
			monthCounter--;
			$('#monthTitle').html(month[thisMonth-1] + " " + yearCounter);
			if(thisMonth-2 < 0){	
				$('#prevmonth').html("<<<|    ");
				$('#nextmonth').html("    |>>>");
			}
			else{
				$('#prevmonth').html("<<<|    ");
				$('#nextmonth').html("    |>>>");
			}
		}
		var selectedMonth = new Month (yearCounter, monthCounter);
		var weeks = selectedMonth.getWeeks();
		createCalendar(weeks);
		checkLogin_events();
	});
})


$(function(){
	$('#nextmonth').click(function(event){
		var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		var monthYear = $('#monthTitle').html().split(" ");
		var thisMonth = month.indexOf(monthYear[0]);
		hideEvents();
		removeCalendar();
		if(thisMonth + 1 > 11){
			monthCounter=0;
			yearCounter++;
			$('#monthTitle').html(month[0] + " " + yearCounter);
			$('#prevmonth').html("<<<|    ");
			$('#nextmonth').html("    |>>>");
		}
		else{
			monthCounter++;
			$('#monthTitle').html(month[thisMonth+1] + " " + yearCounter);
			if(thisMonth+2 > 11){	
				$('#prevmonth').html("<<<|    ");
				$('#nextmonth').html("    |>>>");
			}
			else{
				$('#prevmonth').html("<<<|    ");
				$('#nextmonth').html("    |>>>");
			}
		}
		var selectedMonth = new Month (yearCounter, monthCounter);
		var weeks = selectedMonth.getWeeks();
		createCalendar(weeks);
		checkLogin_events();
	});
})

/* return the current month */
function currentMonth(current){
	var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	$('#monthTitle').html(month[current] + " " + yearCounter);
	return month[current];
}

/* return the previous month */
function previousMonth(current){
	var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	if(current-1 < 0){
		return month[11];
	}
	else{
		return month[current-1];
	}
}

/* return the next month */
function nextMonth(current){
	var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	if(current+1 > 11){
		return month[0];
	}
	else{
		return month[current+1];
	}
}

/* draw the calendar on the DOM */
function createCalendar(weeks) {
	$('body').append("<table class='calendar' id='mainCalendar'></table>");
	var LastDay = 0;
	var isCurrentMonth;
	var stringToAppend;
	for(var row = 0; row<7; row++){
	    stringToAppend+=("<tr>");
	    if (row != 0 && weeks[row-1]!= null) {
	        var days = weeks[row-1].getDates();
	        LastDay = 0;
	    }
	    if (weeks[row-1] == null) {
	        LastDay = 1;
	    }
	    for(var column=0; column<7; column++){
	        if(row == 0){
	        	isCurrentMonth = -1;
	            stringToAppend+=("<td class='weekday'>"+weekday[column]+"</td>");
	        }
	        else{
	            if (days[column] != null && LastDay != 1) {
	            	if (days[column].getDate() == 1){
	            		isCurrentMonth = -isCurrentMonth;
	            	}
	            	if (isCurrentMonth == 1) {
	                	var dateId = parseInt(days[column].getDate()) < 10 ? "0"+days[column].getDate() : days[column].getDate();
	                	var monthId = monthCounter+1 < 10 ? "0"+(monthCounter+1).toString() : monthCounter+1;
	                	stringToAppend+=("<td id='"+yearCounter+monthId+dateId+"'>" + "<p class= 'isCurrentMonth'>" + days[column].getDate() + "</p>" + "</td>");
	            	}
	            	else {
		            	stringToAppend+=("<td class='notCurrentMonth'>" + "<p>" + days[column].getDate() + "</p>" + "</td>");	
	            	}

	            }
	            else {
	                stringToAppend+=("<td class='notCurrentMonth'>" + "</td>");
	            }
	        }
	    }
	    stringToAppend+=("</tr>");
	}
	$('#mainCalendar').append(stringToAppend);
}

function removeCalendar() {
	$('#mainCalendar').remove();
}

/* function to load the events */
function loadEvents() {
	//alert("222");
	var selected_cal = document.getElementById("calendarsHome").options[document.getElementById("calendarsHome").selectedIndex].value;
	var dataString = "selected_cal="+encodeURIComponent(selected_cal);
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "load_events.php", true);
		//alert("555");
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", function(event){
		hideEvents();
		var jsonData = JSON.parse(event.target.responseText);
		window.alert(jsonData);
		$.each(jsonData,function(index, data){
			$("#"+data.event_date).append("<a href='javaScript:void(0);' class='event_item' id='event_id"+data.id+"'>"+data.event_name+"</a>"+"<br class='popup_placeholder'>");
		});
	}, false);
	xmlHttp.send(dataString);
}

/* when user selects a different calendar, call loadEvents */
$(function (){
	$(document).on('change','#calendarsHome', function (){
		loadEvents();
	});
})
/* ajax request for the given event */
/* this will call the showEventPopUp function (below) which displays the details retrieved by this function */
$(document).on('click', '.event_item', function(){
		alert("12321");
	var id = $(this).attr('id').replace( /^\D+/g, '');
	var dataString = "event_id="+id;
	var xmlHttp = new XMLHttpRequest();

	xmlHttp.open("POST", "event_detail.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", function(event){
		var jsonData = JSON.parse(event.target.responseText);
		showEventPopUp(id, jsonData.event_name ,jsonData.event_desc, jsonData.event_date, jsonData.event_time);
	}, false);
	xmlHttp.send(dataString);
}); 



/* remove the events from the calendar */
function hideEvents() {
	$(".event_item").remove();
	$(".popup_placeholder").remove();
}

/* load the events if the user is logged in */
function checkLogin_events() {
	$.ajax({
	url: 'checkLogin.php',
	success: function(check) {
	    if (check == 1) {
	        loadEvents();
	    } 
	}
	});
}


function checkLogin_calendars() {
	$.ajax({
	url: 'checkLogin.php',
	success: function(check) {
	    if (check == 1) {
			
	        loadCalendars();
	    } 
	}
	});
}

/*delete an event from the user */
function deleteEvent() {
	var event_id = $("#popup_event_id").text();
	var dataString = "event_id="+event_id;
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.open("POST", "deleteEvent.php", true);
	xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xmlHttp.addEventListener("load", function(event){
	var jsonData = JSON.parse(event.target.responseText);
	if(jsonData.success){
		hideEvents();
		loadEvents();	
		alert("Success!");
		$('#cancelEvent').trigger('click');
	}
	else{
		alert(jsonData.message);
	}
	}, false);
	xmlHttp.send(dataString);
}
