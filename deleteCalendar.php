<?php
require 'database.php';
header("Content-Type: application/json");
session_start();
$previous_ua = @$_SESSION['useragent'];
$current_ua = $_SERVER['HTTP_USER_AGENT'];
 
if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
	die("Session hijack detected");
}else{
	$_SESSION['useragent'] = $current_ua;
}
if(isset($_SESSION['user_id'])){
	$user_id = $_SESSION['user_id'];
	if(isset($_POST['delete_cal'])){
		$delete_cal = $_POST['delete_cal'];
		if($delete_cal != 'default'){
			$stmt = $mysqli->prepare("select id from calendar where user_id = ? and cal_name = ?");
			$stmt->bind_param('is', $user_id, $delete_cal);
			if(!$stmt){
				echo json_encode(array(
					"success" => false,
					"message" => $mysqli->error
				));
				exit;
			}
			$stmt->execute();
			$stmt->bind_result($cal_id);
			$stmt->fetch();
			$stmt->close();
			$sql = $mysqli->prepare("update events set cal_id = NULL where user_id = ? and cal_id = ?");
			if(!$sql){
				echo json_encode(array(
					"success" => false,
					"message" => $mysqli->error
				));
				exit;
			}
			$sql->bind_param('ii', $user_id, $cal_id);
			$sql->execute();
			$sql->close();
			$drop = $mysqli->prepare("delete from calendar where id = ? and user_id = ? and cal_name = ?");
			if(!$drop){
				echo json_encode(array(
					"success" => false,
					"message" => $mysqli->error
				));
				exit;
			}
			$drop->bind_param('iis', $cal_id, $user_id, $delete_cal);
			$drop->execute();
			$drop->close();
			echo json_encode(array(
				"success" => true,
				"message" => "calendar successfully deleted"
			));
			exit;
		}
		else{
			echo json_encode(array(
				"success" => false,
				"message" => "cannot delete default calendar"
			));
			exit;
		}
	}
}
else{
	echo json_encode(array(
		"success" => false,
		"message" => "you must be logged in to delete a calendar"
	));
	exit;
}

?>