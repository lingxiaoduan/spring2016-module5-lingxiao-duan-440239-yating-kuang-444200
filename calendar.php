<!DOCTYPE html>
<head>
	<?php
	// check for authentification
		session_start();	
		$previous_ua = @$_SESSION['useragent'];
		$current_ua = $_SERVER['HTTP_USER_AGENT'];
		if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
			die("Session failed");
		}else{
			$_SESSION['useragent'] = $current_ua;
		}
	?>
	<!-- include all sources we need to use -->
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="http://classes.engineering.wustl.edu/cse330/content/calendar.js"></script>
	<script type="text/javascript" src="functions.js"></script>
	<link rel="stylesheet" type="css/text" href="style.css"/>
	<meta charset="UTF-8">
	<title>calendar</title>
	<script type='text/javascript' > 
		var monthCounter = 0;
		var yearCounter = 0; 
		// var isLoggedIn = 0;
	</script>	
</head>
<body>

<!-- Adding events -->
<div id="addEventPopUp">
	<input type="text" id="event_name" placeholder="Event Title"/><br>
	<input type="text" id="event_date" placeholder="mm/dd/yyyy"/><br>
	<input type="time" id="event_time"/><br>
	<div>
	<select id="cat">
		<option value = "Birthday">Birthday</option>
		<option value = "Meeting">Meeting</option>
		<option value = "Date">Date</option>
		<option value = "Sports">Sports</option>
	</select>
	</div>
	<textarea cols="40" rows="5" id="event_desc" placeholder="Event description"></textarea><br>
	<div class="calOptionsPopUp">
		<select class='calendars' id='calendarsPopUp'>
			<option value='default'>--</option>
		</select>
	</div><input type="text" id="newCal" placeholder="New Calendar"/><br>
	<button id='addEventButton' type='button'>Add Event</button>
	<button id="cancelAddEvent" type="button">Cancel</button>
</div>

<!-- popup for editting events -->
<!-- shows up when editEvent button clicked -->
<div id="editEventPopUp">
	<input type="text" id="edit_event_name" placeholder="Event Title"/><br>
	<input type="text" id="edit_event_date" placeholder="mm/dd/yyyy"/><br>
	<input type="time" id="edit_event_time"/><br>
	<textarea cols="40" rows="5" id="edit_event_desc" placeholder="Event description"></textarea><br>
	<div class="calOptionsPopUp">
		<select class='calendars' id='edit_calendarsPopUp'>
			<option value='default'>--</option>
		</select>
	</div><input type="text" id="edit_newCal" placeholder="New Calendar"/><br>
	<button id='edit_EventButton' type='button'>Edit Event</button>
	<button id="cancelEditEvent" type="button">Cancel</button>
	<p id="edit_event_id" style="display:none;"></p>
</div>
<!-- end addevent popup -->
<!-- popup for logging in -->
<!-- shows up when login button clicked -->
<div id="loginPopUp">
	<input type='text' name='username' id='username' placeHolder='username'>
	<input type='password' name='password' id='password' placeHolder='password'>
	<button id='loginFormButton' type='button'>Login</button><button id="cancelLogin" type="button">Cancel</button>
</div>

<!-- Register popup -->
<div id="regPopUp">
	<input type='text' name='username' id='user' placeHolder='username'>
	<input type='password' name='password' id='pass' placeHolder='password'>
	<input type='password' name='password' id='repassword' placeHolder='reenter password'>
	<button id='register' type="button">Register</button><button id="cancelreg" type="button">Cancel</button>
</div>


<!-- popup for event details -->
<!-- shows up when event hyperlink clicked -->
<div id="eventPopUp">
	<p id="popup_event_name"></p>
	<p id="popup_event_date"></p>
	<p id="popup_event_desc"></p>
	<p id="popup_event_time"></p>
	<p id="popup_event_id" style="display:none;"></p>
	<button id='editEvent' type='button'>Edit Event</button>
	<button id="cancelEvent" type="button">Cancel</button>
	<button id='DeleteEvent' style="float:right;" type="button" onclick="deleteEvent()">Delete Event</button>
</div>
<!-- end event details popup -->
<button class='addevent' type='button'>Add Event</button>
<div class="calOptionsHome">	
	<select class="calendars" id="calendarsHome">
		<option value='default'>--</option>
	</select>
</div>
<button id="deleteCal" type="button">Delete Calendar</button>
<!-- check if user logged in and display login/logout button accordingly -->
<?php
if(!isset($_SESSION['username'])){
	echo "<button id='loginButton' type='button'>Login</button>";
	echo "<button id='regButton' type='button'>Register</button>";
		echo "<button id='logoutButton' style='display:none' type='button'>Logout</button>";
}
else{
	echo "<button id='loginButton' style ='display:none' type='button'>Login</button>";
	echo "<button id='logoutButton' type='button'>Logout</button><div id='welcome'>Hello, ".$_SESSION['username']."!</div>";
}
?>
<!-- welcome div that displays welcome text for logged in user -->
<div id='welcome2'>

</div>
<!-- end welcome display div -->

<!-- javascript to set and display the header above calendar -->
<script>
	var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var days_31 = ["January", "March", "May", "July", "August", "October", "December"];
	var currentDate = new Date();
	yearCounter = currentDate.getFullYear();
	monthCounter = currentDate.getMonth();
	var selectedMonth = new Month (yearCounter, monthCounter);
	document.write("<div id='monthTitle'>");
	document.write("</div>");
	var currentmonth = currentMonth(currentDate.getMonth());
	document.write("<a href='javaScript:void(0);' id='prevmonth'>" + "<<<|     " + "</a>");
	document.write("<a href='javaScript:void(0);' id='nextmonth'>"+"     |>>>" + "</a>");

	var dayNum = 0;
	var weeks = selectedMonth.getWeeks();

	createCalendar(weeks);
	checkLogin_events();
	checkLogin_calendars();
	
</script>
<!-- end script for calendar header -->

</body>
</html>