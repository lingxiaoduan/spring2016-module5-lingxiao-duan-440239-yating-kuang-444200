
<?php
ini_set("session.cookie_httponly", 1);

require 'database.php';
header("Content-Type: application/json");

if(isset($_POST['username']) && isset($_POST['password'])){
	$user = $_POST['username'];
	$pass_guess = $_POST['password'];
	$stmt = $mysqli->prepare("select id, username, password from login where username = ?");
	if(!$stmt){
		echo json_encode(array(
			"success" => false,
			"message" => "Failed"
		));
		exit;		
	}
	$stmt->bind_param('s', $user);
	$stmt->execute();
	$stmt->bind_result($user_id, $username, $pass_hash);
	$stmt->fetch();
	$stmt->close();
	if($username == $user){
		if(crypt($pass_guess, $pass_hash) == $pass_hash){
			ini_set("session.cookie_httponly", 1);
			session_start();
			$previous_ua = @$_SESSION['useragent'];
			$current_ua = $_SERVER['HTTP_USER_AGENT'];
 
			if(isset($_SESSION['useragent']) && $previous_ua !== $current_ua){
				die("Session failed");
			}else{
				$_SESSION['useragent'] = $current_ua;
			}
			$_SESSION['user_id'] = $user_id;
			$_SESSION['username'] = $username;
			
			$_SESSION['token'] = substr(md5(rand()), 0, 10);
			
			echo json_encode(array(
				"success"=>true,
				"username"=>$username
			));
			exit;
		}
		else{
			echo json_encode(array(
				"success" => false,
				"message" => "invalid username or password"
			));
			exit;
		}
	}
	else{
		echo json_encode(array(
			"success" => false,
			"message" => "invalid username or password"
		));
		exit;
	}
}
else{
	echo json_encode(array(
		"success" => false,
		"message" => "Username or password cannot be blank!"
	));
	exit;
}

?>